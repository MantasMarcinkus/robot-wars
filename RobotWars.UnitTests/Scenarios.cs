﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RobotWars.Library;

namespace RobotWars.UnitTests
{
    [TestClass]
    public class Scenarios : TestBase
    {
        [TestMethod]
        public void Scenarion1()
        {
            var instructions = "MLMRMMMRMMRR".ToCharArray();
            var robot = new Robot(0, 2, Heading.E);
            var game = new Game(5, 5, robot);
            foreach(var instruction in instructions)
            {
                game.MakeAMove(char.ToString(instruction));
            }

            Assert.AreEqual(base.FormatMovementString(4, 1, Heading.N), robot.ReportPosition());
            Assert.AreEqual(base.FormatPenaltiesString(0), robot.ReportPenalties());
        }

        [TestMethod]
        public void Scenarion2()
        {
            var instructions = "LMLLMMLMMMRMM".ToCharArray();
            var robot = new Robot(4, 4, Heading.S);
            var game = new Game(5, 5, robot);
            foreach (var instruction in instructions)
            {
                game.MakeAMove(char.ToString(instruction));
            }

            Assert.AreEqual(base.FormatMovementString(0, 1, Heading.W), robot.ReportPosition());
            Assert.AreEqual(base.FormatPenaltiesString(1), robot.ReportPenalties());
        }

        [TestMethod]
        public void Scenarion3()
        {
            var instructions = "MLMLMLM RMRMRMRM".ToCharArray();
            var robot = new Robot(2, 2, Heading.W);
            var game = new Game(5, 5, robot);
            foreach (var instruction in instructions)
            {
                game.MakeAMove(char.ToString(instruction));
            }

            Assert.AreEqual(base.FormatMovementString(2, 2, Heading.N), robot.ReportPosition());
            Assert.AreEqual(base.FormatPenaltiesString(0), robot.ReportPenalties());
        }

        [TestMethod]
        public void Scenarion4()
        {
            var instructions = "MMLMMLMMMMM".ToCharArray();
            var robot = new Robot(1, 3, Heading.N);
            var game = new Game(5, 5, robot);
            foreach (var instruction in instructions)
            {
                game.MakeAMove(char.ToString(instruction));
            }

            Assert.AreEqual(base.FormatMovementString(0, 0, Heading.S), robot.ReportPosition());
            Assert.AreEqual(base.FormatPenaltiesString(3), robot.ReportPenalties());
        }
    }
}
