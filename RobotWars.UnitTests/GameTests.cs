﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RobotWars.Library;

namespace RobotWars.UnitTests
{
    [TestClass]
    public class GameTests
    {
        [TestMethod]
        public void PenalizeForOutboundingLowerValues()
        {
            var instructions = "M";
            var robot = new Robot(0, 0, Heading.W);
            var game = new Game(5, 5, robot);
            game.MakeAMove(instructions);
            Assert.AreEqual(1, robot.Penalty);
        }

        [TestMethod]
        public void PenalizeForOutboundingMaxValues()
        {
            var instructions = "M";
            var robot = new Robot(0, 5, Heading.N);
            var game = new Game(5, 5, robot);
            game.MakeAMove(instructions);
            Assert.AreEqual(1, robot.Penalty);
        }

        [TestMethod]
        public void PenalizeForOutboundingMaxValuesMultipleTimes()
        {
            var instructions = "MM".ToCharArray();
            var robot = new Robot(0, 5, Heading.N);
            var game = new Game(5, 5, robot);
            foreach (var instruction in instructions)
            {
                game.MakeAMove(char.ToString(instruction));
            }

            Assert.AreEqual(2, robot.Penalty);
        }

        [TestMethod]
        public void PenalizeForOutboundingMaxValuesMultipleTimesWithRotation()
        {
            var instructions = "MRLMLR".ToCharArray();
            var robot = new Robot(0, 4, Heading.N);
            var game = new Game(5, 5, robot);
            foreach(var instruction in instructions)
            {
                game.MakeAMove(char.ToString(instruction));
            }

            Assert.AreEqual(2, robot.Penalty);
        }
    }
}
