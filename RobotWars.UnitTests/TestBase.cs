﻿using RobotWars.Library;

namespace RobotWars.UnitTests
{
    public class TestBase
    {
        internal string FormatMovementString(int x, int y, Heading heading) => $"Position: {x}, {y}, {heading}";

        internal string FormatPenaltiesString(int x) => $"Penalties: {x}";
    }
}
