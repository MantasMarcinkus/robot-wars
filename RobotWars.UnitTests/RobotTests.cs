﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RobotWars.Library;
using System;

namespace RobotWars.UnitTests
{
    [TestClass]
    public class RobotTests : TestBase
    {
        [TestMethod]
        public void RobotHasPositionAndHeadingAndReportsIt()
        {
            IReportable robot = new Robot(0, 0, Heading.N);

            Assert.AreEqual(FormatMovementString(0, 0, Heading.N), robot.ReportPosition());
        }

        [TestMethod]
        public void InstructMoveNorth()
        {
            var robot = new Robot(0, 0, Heading.N);
            robot.Instruct(Instruction.M);
            Assert.AreEqual(FormatMovementString(0, 1, Heading.N), robot.ReportPosition());
        }

        [TestMethod]
        public void InstructMoveEast()
        {
            var robot = new Robot(0, 0, Heading.E);
            robot.Instruct(Instruction.M);
            Assert.AreEqual(FormatMovementString(1, 0, Heading.E), robot.ReportPosition());
        }

        [TestMethod]
        public void InstructMoveSouth()
        {
            var robot = new Robot(0, 1, Heading.S);
            robot.Instruct(Instruction.M);
            Assert.AreEqual(FormatMovementString(0, 0, Heading.S), robot.ReportPosition());
        }

        [TestMethod]
        public void InstructMoveWest()
        {
            var robot = new Robot(1, 0, Heading.W);
            robot.Instruct(Instruction.M);
            Assert.AreEqual(FormatMovementString(0, 0, Heading.W), robot.ReportPosition());
        }

        /// <summary>
        /// Rotate robot around his vertical axis (full circle) counterclockwise
        /// </summary>
        [TestMethod]
        public void InstructRotateLeft()
        {
            var robot = new Robot(0, 0, Heading.N);
            robot.Instruct(Instruction.L);
            Assert.AreEqual(FormatMovementString(0, 0, Heading.W), robot.ReportPosition());
            robot.Instruct(Instruction.L);
            Assert.AreEqual(FormatMovementString(0, 0, Heading.S), robot.ReportPosition());
            robot.Instruct(Instruction.L);
            Assert.AreEqual(FormatMovementString(0, 0, Heading.E), robot.ReportPosition());
            robot.Instruct(Instruction.L);
            Assert.AreEqual(FormatMovementString(0, 0, Heading.N), robot.ReportPosition());
        }

        /// <summary>
        /// Rotate robot around his vertical axis (full circle) clockwise
        /// </summary>
        [TestMethod]
        public void InstructRotateRight()
        {
            var robot = new Robot(0, 0, Heading.N);
            robot.Instruct(Instruction.R);
            Assert.AreEqual(FormatMovementString(0, 0, Heading.E), robot.ReportPosition());
            robot.Instruct(Instruction.R);
            Assert.AreEqual(FormatMovementString(0, 0, Heading.S), robot.ReportPosition());
            robot.Instruct(Instruction.R);
            Assert.AreEqual(FormatMovementString(0, 0, Heading.W), robot.ReportPosition());
            robot.Instruct(Instruction.R);
            Assert.AreEqual(FormatMovementString(0, 0, Heading.N), robot.ReportPosition());
        }

        [TestMethod]
        public void ReportZeroPenalties()
        {
            var robot = new Robot(0, 0, Heading.N);
            Assert.AreEqual(FormatPenaltiesString(0), robot.ReportPenalties());
            Assert.AreEqual(0, robot.Penalty);
        }

        [TestMethod]
        public void ReportOnePenalty()
        {
            var robot = new Robot(0, 0, Heading.N);
            robot.Penalty++;
            Assert.AreEqual(FormatPenaltiesString(1), robot.ReportPenalties());
            Assert.AreEqual(1, robot.Penalty);
        }
    }
}
