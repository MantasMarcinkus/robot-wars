﻿using System;

namespace RobotWars.Library
{
    public class Game
    {
        private Robot robot;

        private Board Board;

        /// <summary>
        /// Main Game Constructor
        /// </summary>
        public Game(int sizeX, int sizeY, Robot robot)
        {
            Board = new Board() { MaxX = sizeX, MaxY = sizeY, MinX = 0, MinY = 0 };
            
            // Check if the robot can be properly set in board he is moving
            // Because setting him at (10;10) when the board is a 5x5 will make it get penalties
            this.robot = robot;
        }

        /// <summary>
        /// Player (now tests) orders robot to move on the game board
        /// So game passes down the instruction to the only robot.
        /// </summary>
        /// <param name="move"></param>
        public void MakeAMove(string move)
        {
            Instruction instruction;
            if(Enum.TryParse<Instruction>(move.ToUpper(), out instruction))
            {
                var predictedMove = this.robot.PredictMove(instruction);

                // If robot would move out of bounds we should not let him and penalize him.
                if(predictedMove.X < Board.MinX || 
                    predictedMove.Y < Board.MinY || 
                    predictedMove.X >= Board.MaxX || 
                    predictedMove.Y >= Board.MaxY)
                {
                    this.robot.Penalty++;
                }
                else
                {
                    this.robot.Instruct(instruction);
                }
            }
            else
            {
                // Ignore invalid moves
            }
        }
    }
}
