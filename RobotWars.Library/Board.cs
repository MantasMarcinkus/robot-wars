﻿namespace RobotWars.Library
{
    /// <summary>
    /// Board class. Can be extended to three dimensions
    /// </summary>
    public class Board
    {
        public int MaxX { get; set; }

        public int MaxY { get; set; }

        public int MinX { get; set; }

        public int MinY { get; set; }
    }
}
