﻿namespace RobotWars.Library
{
    /// <summary>
    /// Robot Class. No inheritance used because there is no point. 
    /// If such need appears - create class that inherits from this one and override/extend functionality
    /// </summary>
    public class Robot : IInstructable, IReportable
    {
        Heading[] MovementCapabilites = new Heading[] { Heading.N, Heading.E, Heading.S, Heading.W };

        internal Heading Heading { get; private set; }

        /// <summary>
        /// Has X and Y coordinates
        /// </summary>
        internal Position Position = new Position();

        internal static readonly int StepSize = 1;

        /// <summary>
        /// I Assume that Robot have to know if it is penalised, 
        /// not the game should calculate it's score
        /// This looks like hit points in games - 
        /// If you have more than that, Robot must die, not the game should kill it.
        /// </summary>
        public int Penalty { get; set; } = 0;

        /// <summary>
        /// Position of the robot
        /// </summary>
        internal string PositionText => $"Position: {this.Position.X}, {this.Position.Y}, {this.Heading}";

        /// <summary>
        /// Robot constructor
        /// </summary>
        /// <param name="x">Initial X position</param>
        /// <param name="y">Initial Y position</param>
        /// <param name="heading">Initial Heading</param>
        public Robot(int x, int y, Heading heading)
        {
            this.Position.X = x;
            this.Position.Y = y;
            this.Heading = heading;
        }

        /// <summary>
        /// Instructs robot to Move or rotate Left or Right
        /// </summary>
        /// <param name="instruction">Move, Left, Right</param>
        public void Instruct(Instruction instruction)
        {
            switch (instruction)
            {
                case Instruction.L: RotateLeft(); break;
                case Instruction.R: RotateRight(); break;
                case Instruction.M: MakeAStep(); break;
            }
        }

        /// <summary>
        /// This method is for Robot to report where it is going, just that
        /// game runner would decide if it is okay to move there
        /// </summary>
        /// <param name="instruction">
        ///     <paramref name="Instruction.M"/> only, because rotating is not dangerous for robot
        /// </param>
        /// <returns></returns>
        public Position PredictMove(Instruction instruction)
        {
            switch (instruction)
            {
                case Instruction.M:
                    // Creating new Predictable position which will be returned to Game runner
                    return Move(new Position() { X = this.Position.X, Y = this.Position.Y });
                default:
                    return this.Position;
            }
        }

        /// <summary>
        /// One step for this robot, huge step if it's set above <paramref name="StepSize"/>
        /// </summary>
        private void MakeAStep()
        {
            Move(this.Position);
        }

        private Position Move(Position position)
        {
            switch (this.Heading)
            {
                case Heading.N: position.Y += StepSize; break;
                case Heading.S: position.Y -= StepSize; break;
                case Heading.E: position.X += StepSize; break;
                case Heading.W: position.X -= StepSize; break;
            }

            return position;
        }

        /// <summary>
        /// Method describes how movement is described in this universe
        /// </summary>
        private void RotateLeft()
        {
            var result = ((int)Heading - 1) % MovementCapabilites.Length;
            
            // When turning left we return to the end of the array.
            this.Heading = MovementCapabilites[result == - 1 ? MovementCapabilites.Length - 1 : result];
        }

        /// <summary>
        /// Method describes how movement is described in this universe
        /// </summary>
        private void RotateRight()
        {
            this.Heading = MovementCapabilites[((int)Heading + 1) % MovementCapabilites.Length];
        }

        /// <summary>
        /// Reports position of the robot. Information is taken from internal property
        /// </summary>
        /// <returns></returns>
        public string ReportPosition() { return PositionText; }

        /// <summary>
        /// Reports Penalties Score
        /// </summary>
        /// <returns></returns>
        public string ReportPenalties() { return $"Penalties: {Penalty}"; }
    }
}
