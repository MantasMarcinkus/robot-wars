﻿namespace RobotWars.Library
{
    public interface IInstructable
    {
        void Instruct(Instruction instruction);

        Position PredictMove(Instruction instruction);
    }
}
