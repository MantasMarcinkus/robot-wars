﻿namespace RobotWars.Library
{
    public interface IReportable
    {
        string ReportPosition();
    }
}
